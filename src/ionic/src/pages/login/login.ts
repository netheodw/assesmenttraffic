import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, IonicPage } from 'ionic-angular';
import { RegisterPage, EventsListPage } from '../pages';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { JavaApi } from '../../shared/java-api';
  
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  appName: string = "Mobile Traffic";
  loading: Loading;
  usersCredential = {
    username: "",
    password: ""
  };
 
  constructor(private nav: NavController, 
              private alertCtrl: AlertController, 
              private loadingCtrl: LoadingController,
              private httpClient: HttpClient,
              private javaApi: JavaApi) {}
 
  public createAccount() {
    this.nav.push(RegisterPage);
  }
 
  public login(username: string, password: string) {
    this.showLoading();
    this.javaApi.userLogin(username, password).subscribe(data=>{
      console.log(data);
      if(data.status == 200){
        this.usersCredential.password = "";
        this.usersCredential.username = "";
        this.nav.push(EventsListPage);
      }else{
        this.showError("Invalid username or password");
      }
    }, (error)=>{
      let e = JSON.parse(error.text());
      this.showError(e.message);
    });    
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Something went wrong!!!',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
}
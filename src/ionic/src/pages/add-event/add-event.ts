import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { JavaApi } from '../../shared/java-api';
 
/**
 * Generated class for the AddEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@Component({
  selector: 'page-add-event',
  templateUrl: 'add-event.html',
})
export class AddEventPage {
  
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  issue: string = "";
  position : {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public toastCtrl: ToastController, public javaApi: JavaApi) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEventPage');

    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      console.log("lat: " + resp.coords.latitude + ", lng: " + resp.coords.longitude);
      this.position = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      this.loadMap(resp.coords.latitude, resp.coords.longitude);
     }).catch((error) => { 
       console.log('Error getting location', error);
     });
  }

  register(){
    console.log("register event");
    console.log(this.issue);
    this.javaApi.addReport(this.position, this.issue)
      .subscribe(data=>{
        console.log(data);
        let message: string = "";
        if(data == "OK"){
          this.navCtrl.pop();
          message = "Report registered successfully";
        }else{
          message = "Something went wrong";
        }
        this.presentToast(message);
      });
  }

  loadMap(lat, lon){
    let latLng = new google.maps.LatLng(lat, lon);
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    let marker = new google.maps.Marker({position: latLng, title: 'Position: ' + lat + " - " + lon});
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    marker.setMap(this.map);
  }

  presentToast(message: string){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { IReport } from '../../interfaces/report';
import { IUser } from '../../interfaces/user';
import { JavaApi } from '../../shared/java-api'; 

/**
 * Generated class for the EventDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@Component({
  selector: 'page-event-details',
  templateUrl: 'event-details.html',
})
export class EventDetailsPage {

  report: IReport;
  user = {} as  IUser;

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl : ToastController, public javaApi: JavaApi) {
    this.report = this.navParams.get("report");
    console.log("asda");
    this.javaApi.getUserById(this.report.userId).subscribe(data=>{
      this.user = data;
      console.log(data);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventDetailsPage');
    this.loadMap();
  }

  loadMap(){
    let latLng = new google.maps.LatLng(this.report.lat, this.report.lng);
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    let marker = new google.maps.Marker({position: latLng, title: 'Position: ' + this.report.lat + " - " + this.report.lng});
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    marker.setMap(this.map);
  }

  rate(action: string){
    this.javaApi.rateReport(action, this.report.id).subscribe(data=>{
      console.log(data);
      let message : string = "";
      if(data == "OK"){
        message = "Your rating was registered successfully: " + action;
      }else{
        message = "Something went wrong";
      }
      this.presentToast(message);
    });
  }

  presentToast(message: string){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}

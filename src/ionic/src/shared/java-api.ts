import { Injectable }  from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs';
import { Observable } from 'rxjs/Observable';
import { JwtHelper } from '../helpers/jwt';
import { IUser } from '../interfaces/user';

@Injectable()
export class JavaApi{

    private baseUrl: string = 'http://192.168.1.29:8090';
    private token: string = "";
    private currentUser = {} as IUser;

    constructor(private httpClient: HttpClient, private http: Http) { }

    userLogin(username: string, password: string) : Observable<any>{
        let response: Response;
        let data : any = {
            "username":username,
            "password":password 
        };
        this.token = "";

        return this.http.post(`${this.baseUrl}/api/users/login`, data)
            .map((response:Response)=>{
                console.log(response);
                this.token = response.text();
                let jwtHelper = new JwtHelper();
                let parsedToken = jwtHelper.decodeToken(this.token);
                this.setCurrentUser(parsedToken.user);
                return response;
            });
    }

    getReports(): Observable<any>{
        return this.http.get(`${this.baseUrl}/api/reports`, {headers: this.getHeader()})
            .map((response: Response)=>{
                return response.json();
            });
    }

    getUserByUsername(username: string) : Observable<any>{
        return this.http.get(`${this.baseUrl}/api/users/` + username, {headers: this.getHeader()})
            .map((response : Response)=>{
                return response.json();
            });
    }

    getUserById(userId: number){
        return this.http.get(`${this.baseUrl}/api/users/get/` + userId, {headers: this.getHeader()})
            .map((response: Response)=>{
                return response.json();
            })
    }

    addReport(position: any, issue: string){
        let headers : Headers =  new Headers({'authorization': 'Bearer ' + this.token,
                                              'content-type': 'application/json'});
        /* headers.append('authorization', 'Bearer ' + this.token);
        headers.append('content-type', 'application/json'); */

        let data = {
            issue: issue,
            lat: position.lat,
            lng: position.lng
        };
        return this.http.post(`${this.baseUrl}/api/reports`, data, {headers: headers})
            .map((response: Response)=>{
                console.log("return " + response);
                return response.json();
            });
    }

    rateReport(action: string, reportId: number) : Observable<any>{
        let headers : Headers =  new Headers({'authorization': 'Bearer ' + this.token,
                                              'content-type': 'application/json'});

        let data = {
            "reportId":reportId,
            "assessment":action
        }

        return this.http.post(`${this.baseUrl}/api/assessments`, data, {headers : headers})
            .map((response : Response) =>{
                console.log(response);
                return response.json();
            });
    }

    getAssessmentsByReportId(reportId : number){
        return this.http.get (`${this.baseUrl}/api/assessments/` + reportId, {headers: this.getHeader()})
            .map((response : Response)=>{
                //console.log(response);
                return response.json();
            })
    }

    getHeader() : Headers{
        return new Headers({'authorization': 'Bearer ' + this.token});
    }

    setCurrentUser(user : any){
        this.currentUser.id = user.id;
        this.currentUser.username = user.username;
        this.currentUser.password = user.password;
        this.currentUser.name = user.name;
        this.currentUser.surname = user.surname;
        this.currentUser.date_of_birthday =  user.date_of_birthday;
    }

    getCurrentUSer() : IUser{
        return this.currentUser;
    }
}
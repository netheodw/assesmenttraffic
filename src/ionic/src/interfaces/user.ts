export interface IUser{
    id: number;
    username: string;
    password: string;
    name: string;
    surname: string;
    date_of_birthday: Date;
}


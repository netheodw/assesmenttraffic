package com.eap.assesmenttraffic.config;

import com.eap.assesmenttraffic.services.UserManagement;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import javax.sql.DataSource;

@SpringBootApplication
@EnableAutoConfiguration(exclude = RepositoryRestMvcAutoConfiguration.class)

// Apis
@ComponentScan(basePackages = "com.eap.assesmenttraffic.api")

// Services
@ComponentScan(basePackages = "com.eap.assesmenttraffic.services")


@EntityScan(basePackages = "com.eap.assesmenttraffic.models")
@EnableJpaRepositories(basePackages= "com.eap.assesmenttraffic.repositories")
public class TrafficApplication  {

	@Bean
	public DataSource dataSource() throws Exception {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/traffic?useSSL=false");
		ds.setUsername("root");
		ds.setPassword("abcd1234");
		return ds;
	}

	@Bean
	public UserManagement userManagement() {
		return new UserManagement();
	}

    public static void main(String[] args) {
		SpringApplication.run(TrafficApplication.class, args);
	}
}
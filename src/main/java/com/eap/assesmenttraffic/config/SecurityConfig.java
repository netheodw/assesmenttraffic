package com.eap.assesmenttraffic.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    public static final String DEF_USERS_BY_USERNAME_QUERY =
            "select username, password" +
            "from users " +
            "where username = ?";

    public static final String DEF_AUTHORITIES_BY_USERNAME_QUERY =
            "select username, roles.name as role" +
            "from user_roles " +
            "left join users on user_roles.user_id = users.id " +
            "left join roles on roles.id = user_roles.role_id"+
            "where users.username = ?";


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth
                .jdbcAuthentication()
                    .dataSource(dataSource)
                    .usersByUsernameQuery(DEF_USERS_BY_USERNAME_QUERY)
                    .authoritiesByUsernameQuery(DEF_AUTHORITIES_BY_USERNAME_QUERY);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/api/users/login", "/api/users");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
                http
                    .formLogin()
                    .   disable()
                    .csrf()
                        .disable();

                http
                .addFilterBefore(new Filter() {
                    @Override
                    public void init(FilterConfig filterConfig) throws ServletException {
                    }

                    @Override
                    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
                        final HttpServletRequest request = (HttpServletRequest) req;
                        final HttpServletResponse response = (HttpServletResponse) res;
                        final String authHeader = request.getHeader("authorization");

                        if ("OPTIONS".equals(request.getMethod())) {
                            response.setStatus(HttpServletResponse.SC_OK);
                            chain.doFilter(req, res);
                        } else {
                            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                                throw new ServletException("Missing or invalid Authorization header");
                            }

                            final String token = authHeader.substring(7);
                            try {
                                final Claims claims = Jwts.parser().setSigningKey("abcd1234").parseClaimsJws(token).getBody();
                                List<String> roles = (ArrayList<String>) claims.get("roles");
                                LinkedHashMap user = (LinkedHashMap<String, Object>) claims.get("user");
                                req.setAttribute("user_id", user.get("id"));
                                List<SimpleGrantedAuthority> authorities = new ArrayList<>();
                                roles.forEach(r -> authorities.add(new SimpleGrantedAuthority(r)));
                                User principal = new User(claims.getSubject(), "", authorities);
                                UsernamePasswordAuthenticationToken t
                                        = new UsernamePasswordAuthenticationToken(principal, "", authorities);
                                SecurityContextHolder.getContext().setAuthentication(t);
                            } catch (Exception e) {
                                throw new ServletException("Invalid Token");
                            }

                            chain.doFilter(req, res);
                        }
                    }

                    @Override
                    public void destroy() {
                    }
                }, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/api/users/role").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/users/{userId}").hasAuthority("ADMIN");
    }

}

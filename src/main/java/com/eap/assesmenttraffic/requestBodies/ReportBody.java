package com.eap.assesmenttraffic.requestBodies;

public class ReportBody {
    public Integer id;
    public String issue;
    public float lat;
    public float lng;
}

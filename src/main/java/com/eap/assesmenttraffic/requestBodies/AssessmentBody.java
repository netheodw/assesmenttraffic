package com.eap.assesmenttraffic.requestBodies;

import com.eap.assesmenttraffic.models.Assessment;

public class AssessmentBody {
    public long id;
    public long reportId;
    public Assessment.AssessmentType assessment;
}

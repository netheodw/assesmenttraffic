package com.eap.assesmenttraffic.repositories;


import com.eap.assesmenttraffic.models.User;
import com.eap.assesmenttraffic.models.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
    @Query("SELECT ur FROM UserRole ur WHERE ur.user_id = :userId")
    ArrayList<UserRole> findAllByUserId(@Param("userId") long userId);

    @Query("SELECT ur FROM UserRole ur WHERE ur.user_id = :userId and ur.role_id = :roleId")
    UserRole findUserRoleByUserIdAndRoleId(@Param("userId") long userId, @Param("roleId") long roleId);
}

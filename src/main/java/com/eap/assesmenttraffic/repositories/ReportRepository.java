package com.eap.assesmenttraffic.repositories;


import com.eap.assesmenttraffic.models.Report;
import com.eap.assesmenttraffic.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReportRepository extends CrudRepository<Report, Long> {
}

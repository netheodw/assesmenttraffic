package com.eap.assesmenttraffic.repositories;

import com.eap.assesmenttraffic.models.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface RoleRepository extends CrudRepository<Role, Long> {
    @Query("SELECT r FROM Role r WHERE r.name = :name")
    Role findByName(@Param("name") String name);

    @Query("SELECT r.name from Role r where r.id in :ids")
    ArrayList<String> findRolesByIds(@Param("ids") ArrayList<Long> ids);
}

package com.eap.assesmenttraffic.repositories;

import com.eap.assesmenttraffic.models.Assessment;
import com.eap.assesmenttraffic.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AssessmentRepository extends CrudRepository<Assessment, Long> {

    @Query("SELECT a FROM Assessment a WHERE a.report_id = :id")
    List<Assessment> findByReportId(@Param("id") long id);
}

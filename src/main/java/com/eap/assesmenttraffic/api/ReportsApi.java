package com.eap.assesmenttraffic.api;

import com.eap.assesmenttraffic.models.Report;
import com.eap.assesmenttraffic.requestBodies.ReportBody;
import com.eap.assesmenttraffic.services.ReportManagement;
import com.eap.assesmenttraffic.services.UserManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RequestMapping("/api/reports")
@RestController
public class ReportsApi {

    @Autowired
    ReportManagement reportManagemnet;

    @Autowired
    UserManagement userManagement;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAll () {
        List<Report> events = reportManagemnet.getAllReports();
        return new ResponseEntity<>(events, null, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity post (HttpServletRequest request, @RequestBody ReportBody eventBody) {
        Integer userId =  (Integer) request.getAttribute("user_id");
        if (!reportManagemnet.registerReport(userId, eventBody.issue, eventBody.lat, eventBody.lng)) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed to register the event");
        }
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity put (HttpServletRequest request, @RequestBody ReportBody eventBody){
        Report report = reportManagemnet.getReport(eventBody.id);
        if (report == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Event not found");
        }
        long dbUserId = report.getUserId();
        if (!userManagement.hasAccessToUpdateOrDeleteRecord(request, dbUserId)) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The user does not have the privilege to modify the event.");
        }

        if (!reportManagemnet.updateReport(report.getId(), report.getUserId(), eventBody.issue, eventBody.lat, eventBody.lng)){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed to update the event");
        }

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(path="/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete (HttpServletRequest request, @PathVariable("id") long id){
        Report event = reportManagemnet.getReport(id);
        if (event == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Event not found");
        }

        long dbUserId = event.getUserId();
        if (!userManagement.hasAccessToUpdateOrDeleteRecord(request, dbUserId)) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The user does not have the privilege to modify the event.");
        }

        if (!reportManagemnet.deleteReport(id)){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed to delete the event");
        }
        return ResponseEntity.ok(HttpStatus.OK);
    }

}

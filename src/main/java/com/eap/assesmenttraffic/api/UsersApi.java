package com.eap.assesmenttraffic.api;

import com.eap.assesmenttraffic.models.Role;
import com.eap.assesmenttraffic.models.User;
import com.eap.assesmenttraffic.requestBodies.UserRoleBody;
import com.eap.assesmenttraffic.services.UserManagement;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.rmi.ServerException;
import java.util.List;

@CrossOrigin
@RequestMapping(path ="/api/users")
@RestController
public class UsersApi {

    @Autowired
    private UserManagement userManagement;

    @RequestMapping(path="/login", method = RequestMethod.POST)
    public String login(@RequestBody User user) throws ServerException {

        if (!userManagement.checkUser(user)) {
            throw new ServerException("Invalid login. Username or Password is not valid.");
        }

        User dbUser = userManagement.getUserByUserName(user.getUsername());
        List<String> roles = userManagement.getRoles(dbUser.getId());
        return Jwts.builder()
                .setSubject(user.getUsername())
                .claim("roles", roles)
                .claim("user", dbUser)
                .signWith(SignatureAlgorithm.HS256, "abcd1234")
                .compact();

    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody User user){
        if (userManagement.registerUser(user)) {
            return ResponseEntity.ok(HttpStatus.OK);
        }
        return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(path="/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable("userId") long userId) {
        if (userManagement.deleteUser(userId)) {
            return ResponseEntity.ok(HttpStatus.OK);
        }
        return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(path="/role", method = RequestMethod.POST)
    public ResponseEntity setRole(@RequestBody UserRoleBody userRoleBody) {
        User user = userManagement.getUserByUserName(userRoleBody.username);
        if (user == null){
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }

        Role role = userManagement.getRoleByName(userRoleBody.role);
        if (role == null) {
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }

        if (!userManagement.registerUserRole(user.getId(), role.getId())) {
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(path="/role", method = RequestMethod.DELETE)
    public ResponseEntity deleteRole(@RequestBody UserRoleBody userRoleBody) {
        User user = userManagement.getUserByUserName(userRoleBody.username);
        if (user == null){
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }

        Role role = userManagement.getRoleByName(userRoleBody.role);
        if (role == null) {
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }

        if (!userManagement.deleteUserRole(user.getId(), role.getId())) {
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(path="/get/{id}", method = RequestMethod.GET)
    public User getUserById(@PathVariable("id") long id) {
        return userManagement.getUserById(id);
    }
}

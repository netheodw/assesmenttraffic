package com.eap.assesmenttraffic.api;

import com.eap.assesmenttraffic.models.Assessment;
import com.eap.assesmenttraffic.requestBodies.AssessmentBody;
import com.eap.assesmenttraffic.services.AssessmentManagement;
import com.eap.assesmenttraffic.services.UserManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RequestMapping("/api/assessments")
@RestController
public class AssessmentsApi {

    @Autowired
    AssessmentManagement assessmentManagement;

    @Autowired
    UserManagement userManagement;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAll (HttpServletRequest request){
        List<Assessment> assessments = assessmentManagement.getAllAssessments();
        return new ResponseEntity<>(assessments, null, HttpStatus.OK);
    }

    @RequestMapping(path = "/{report_id}", method = RequestMethod.GET)
    public List<Assessment> getAssessmentsByReportID (@PathVariable("report_id") long reportId){
        List<Assessment> assessments = assessmentManagement.getAssessmentsByID(reportId);
        return  assessments;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity post (HttpServletRequest request, @RequestBody AssessmentBody assessmentBody){
        Integer userId =  (Integer) request.getAttribute("user_id");
        if (!assessmentManagement.registerAssessment(userId, assessmentBody.reportId, assessmentBody.assessment)){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed to register the assessment");
        }
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity put (HttpServletRequest request, @RequestBody AssessmentBody assessmentBody){
        Assessment assesment = assessmentManagement.getAssesment(assessmentBody.id);
        if (assesment == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Assessment not found");
        }
        long dbUserId = assesment.getUserId();
        if (!userManagement.hasAccessToUpdateOrDeleteRecord(request, dbUserId)) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The user does not have the privilege to modify the assessment.");
        }

        if (!assessmentManagement.updateAssessment(assesment.getId(), assesment.getUserId(), assessmentBody.reportId, assessmentBody.assessment)){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed to update the assessment");
        }

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(path="/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete (HttpServletRequest request, @PathVariable("id") long id){
        Assessment assesment = assessmentManagement.getAssesment(id);
        if (assesment == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Event not found");
        }

        long dbUserId = assesment.getUserId();
        if (!userManagement.hasAccessToUpdateOrDeleteRecord(request, dbUserId)) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The user does not have the privilege to modify the assessment.");
        }

        if (!assessmentManagement.deleteAssessmnent(id)){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Failed to delete the assessment");
        }
        return ResponseEntity.ok(HttpStatus.OK);
    }


}

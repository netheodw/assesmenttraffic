package com.eap.assesmenttraffic.services;

import com.eap.assesmenttraffic.models.Report;
import com.eap.assesmenttraffic.repositories.AssessmentRepository;
import com.eap.assesmenttraffic.repositories.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Component
public class ReportManagement {

    @Autowired
    private ReportRepository reportRepository;

    public List<Report> getAllReports() {
        List<Report> reports = (List<Report>) reportRepository.findAll();
        return reports;
    }

    public boolean registerReport(long userId, String issue, float lat, float lng) {
        try {
            Report report = new Report();
            report.setUserId(userId);
            report.setIssue(issue);
            report.setLat(lat);
            report.setLng(lng);
            reportRepository.save(report);
            return true;
        }
        catch(Exception e) {
            return false;
        }
    }

    public boolean deleteReport(long id) {
        try {
            reportRepository.delete(id);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean updateReport(long id, long userId, String issue, float lat, float lng) {
        try {
            Report data = new Report();
            data.setId(id);
            data.setUserId(userId);
            data.setIssue(issue);
            Date date = new Date();
            data.setIssueDate(new Timestamp(date.getTime()));
            data.setLng(lng);
            data.setLat(lat);
            reportRepository.save(data);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public Report getReport(long reportId) {
       return reportRepository.findOne(reportId);
    }

}


package com.eap.assesmenttraffic.services;

import com.eap.assesmenttraffic.models.Assessment;
import com.eap.assesmenttraffic.repositories.AssessmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AssessmentManagement {

    @Autowired
    private AssessmentRepository assessmentRepository;

    public List<Assessment> getAllAssessments() {
        List<Assessment> assessments = (List<Assessment>) assessmentRepository.findAll();
        return assessments;
    }

    public boolean registerAssessment(long userId, long reportId, Assessment.AssessmentType assessmentType) {
        try{
            Assessment assessment = new Assessment();
            assessment.setUserId(userId);
            assessment.setReportId(reportId);
            assessment.setAssessment(assessmentType);
            assessmentRepository.save(assessment);
            return true;
        }
        catch(Exception e) {
            return false;
        }
    }


    public boolean deleteAssessmnent(long id) {
        try {
            assessmentRepository.delete(id);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean updateAssessment(long id, long userId, long reportId, Assessment.AssessmentType type) {
        try {
            Assessment assessment = new Assessment();
            assessment.setId(id);
            assessment.setUserId(userId);
            assessment.setReportId(reportId);
            assessment.setAssessment(type);
            assessmentRepository.save(assessment);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public Assessment getAssesment(long id){
        return assessmentRepository.findOne(id);
    }

    public List<Assessment> getAssessmentsByID(long id){

        List<Assessment> assessments = assessmentRepository.findByReportId(id);
        return assessments;
    }

}

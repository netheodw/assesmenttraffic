package com.eap.assesmenttraffic.services;


import com.eap.assesmenttraffic.models.Role;
import com.eap.assesmenttraffic.models.User;
import com.eap.assesmenttraffic.models.UserRole;
import com.eap.assesmenttraffic.repositories.RoleRepository;
import com.eap.assesmenttraffic.repositories.UserRepository;
import com.eap.assesmenttraffic.repositories.UserRoleRepository;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Component;
import org.xmlpull.v1.builder.Iterable;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserManagement {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    public UserManagement(){

    }

    public boolean registerUser(User user) {
        try {
            userRepository.save(user);

            UserRole userRole = new UserRole();
            userRole.setUserid(user.getId());
            userRole.setRoleId(2);
            userRoleRepository.save(userRole);
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }

    public boolean checkUser(User user) {
        try {
            Object result = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
            return result instanceof User;
        } catch (Exception ex) {
            return false;
        }
    }

    public User getUserByUserName(String username) {
        try {
            return userRepository.findByUsername(username);
        }
        catch (Exception ex) {
            return null;
        }
    }

    public Role getRoleByName(String name) {
        try {
            return roleRepository.findByName(name);
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean registerUserRole(long userId, long roleId){
        try {
            UserRole userRole = new UserRole();
            userRole.setRoleId(roleId);
            userRole.setUserid(userId);
            userRoleRepository.save(userRole);
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }

    public List<String> getRoles(long userId) {
        List<String> roles = new ArrayList<>();
        try {
            ArrayList<UserRole> userRoles = userRoleRepository.findAllByUserId(userId);
            if (userRoles.isEmpty()) {
                return roles;
            }

            List<Long> ids = userRoles.stream()
                    .map(userRole -> userRole.getRoleId())
                    // Make a list.
                    .collect(Collectors.toList());

            roles = roleRepository.findRolesByIds(new ArrayList<>(ids));

            return roles;
        }
        catch (Exception ex) {
            return roles;
        }
    }

    public boolean deleteUserRole(long userId, long roleId) {
        try {
            UserRole userRole = userRoleRepository.findUserRoleByUserIdAndRoleId(userId, roleId);
            userRoleRepository.delete(userRole.getId());
            return  true;
        }
        catch (Exception ex) {
            return false;
        }
    }

    public boolean hasAccessToUpdateOrDeleteRecord(HttpServletRequest request, long userId) {
        try {
            Integer reqUserId = (Integer) request.getAttribute("user_id");
            List<String> reqRoles = (ArrayList<String>)request.getAttribute("roles");
            return reqUserId == userId
                    || reqRoles.indexOf("ADMIN") != -1;
        }
        catch (Exception ex) {
            return false;
        }
    }

    public boolean deleteUser(long userId) {
        try {
            userRepository.delete(userId);
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }


    public User getUserById(long id) {
        return userRepository.findById(id);
    }
}

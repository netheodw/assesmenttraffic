package com.eap.assesmenttraffic.models;

import javax.persistence.*;

@Entity
@Table(name = "user_roles", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"user_id", "role_id"})
})
public class UserRole {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    private long user_id;
    private long role_id;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return user_id;
    }
    public void setUserid(long user_id) {
        this.user_id = user_id;
    }


    public long getRoleId() {
        return role_id;
    }
    public void setRoleId(long role_id) {
        this.role_id = role_id;
    }

}

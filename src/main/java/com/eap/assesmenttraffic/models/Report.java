package com.eap.assesmenttraffic.models;

import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "reports")
public class Report {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @Column(name="user_id")
    private long user_id;

    @Column(name="issue")
    private String issue;

    @Column(name="lat")
    private float lat;

    @Column(name="lng")
    private float lng;

    @Column(name="issue_date")
    @CreationTimestamp
    private Timestamp issue_date;

    public long getId(){
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return user_id;
    }
    public void setUserId(long userId){
        this.user_id = userId;
    }


    public String getIssue(){
        return this.issue;
    }
    public void setIssue(String issue) {
        this.issue = issue;
    }

    public float getLat(){
        return this.lat;
    }
    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng(){
        return this.lng;
    }
    public void setLng(float lng) {
        this.lng = lng;
    }

    public Timestamp getIssueDate() {
        return this.issue_date;
    }
    public void setIssueDate(Timestamp issueDate){
        this.issue_date = issueDate;
    }

}

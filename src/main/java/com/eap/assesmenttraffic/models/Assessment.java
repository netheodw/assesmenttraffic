package com.eap.assesmenttraffic.models;

import javax.persistence.*;

@Entity
@Table(name = "assessments")
public class Assessment {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private long report_id;
    private long user_id;

    @Column(name="assessment")
    @Enumerated(EnumType.STRING)
    private AssessmentType assessment;

    public enum AssessmentType {
        DISLIKE("DISLIKE"),  NONE("NONE"), LIKE("LIKE") ;
        private String value;
        AssessmentType(String value) { this.value = value; }
        public String getValue() { return value; }
    }

    public long getId(){
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return user_id;
    }
    public void setUserId(long userId){
        this.user_id = userId;
    }

    public long getReportId(){
        return this.report_id;
    }
    public void setReportId(long eventId) {
        this.report_id = eventId;
    }

    public AssessmentType getAssessment(){
        return this.assessment;
    }
    public void setAssessment(AssessmentType assessment) {
        this.assessment = assessment;
    }
}

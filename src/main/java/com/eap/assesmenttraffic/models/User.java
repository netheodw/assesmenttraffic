package com.eap.assesmenttraffic.models;

import com.fasterxml.jackson.annotation.JsonFormat;


import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private long id;

  @Column(unique = true)
  private String username;
  private String password;
  private String name;
  private String surname;

  @Column(name = "date_of_birthday", columnDefinition="DATE")
  @JsonFormat(pattern="yyyy-MM-dd")
  private Date date_of_birthday;

  public long getId() {
    return id;
  }
  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }
  public void setSurname(String surname) {
    this.surname = surname;
  }

  public Date getDateOfBirthday() {
    return date_of_birthday;
  }
  public void setDateOfBirthday(Date dateOfBirthday) {
    this.date_of_birthday = dateOfBirthday;
  }

}

-- --------------------------------------------------------
-- Διακομιστής:                  127.0.0.1
-- Έκδοση διακομιστή:            8.0.11 - MySQL Community Server - GPL
-- Λειτ. σύστημα διακομιστή:     Win64
-- HeidiSQL Έκδοση:              9.5.0.5278
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for traffic
CREATE DATABASE IF NOT EXISTS `traffic` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `traffic`;

-- Dumping structure for πίνακας traffic.assessments
CREATE TABLE IF NOT EXISTS `assessments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `assessment` enum('NONE','LIKE','DISLIKE') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `assessments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table traffic.assessments: ~44 rows (approximately)
/*!40000 ALTER TABLE `assessments` DISABLE KEYS */;
INSERT INTO `assessments` (`id`, `user_id`, `assessment`, `report_id`) VALUES
	(1, 16, 'LIKE', 2),
	(2, 16, 'LIKE', 1),
	(3, 16, 'LIKE', 1),
	(4, 16, 'LIKE', 2),
	(5, 16, 'LIKE', 2),
	(6, 16, 'LIKE', 1),
	(7, 16, 'LIKE', 1),
	(8, 16, 'DISLIKE', 2),
	(9, 16, 'LIKE', 1),
	(10, 16, 'LIKE', 1),
	(11, 16, 'DISLIKE', 1),
	(12, 16, 'LIKE', 1),
	(13, 16, 'DISLIKE', 1),
	(14, 16, 'LIKE', 1),
	(15, 16, 'LIKE', 1),
	(16, 16, 'LIKE', 1),
	(17, 16, 'DISLIKE', 1),
	(18, 16, 'LIKE', 1),
	(19, 16, 'LIKE', 5),
	(20, 16, 'LIKE', 5),
	(21, 16, 'DISLIKE', 5),
	(22, 16, 'LIKE', 5),
	(23, 16, 'LIKE', 5),
	(24, 16, 'LIKE', 5),
	(25, 16, 'LIKE', 5),
	(26, 16, 'LIKE', 4),
	(27, 16, 'LIKE', 1),
	(28, 16, 'LIKE', 30),
	(29, 16, 'LIKE', 2),
	(30, 16, 'LIKE', 2),
	(31, 16, 'LIKE', 30),
	(32, 16, 'LIKE', 1),
	(33, 16, 'LIKE', 6),
	(34, 16, 'LIKE', 30),
	(35, 16, 'LIKE', 30),
	(36, 16, 'DISLIKE', 31),
	(37, 16, 'LIKE', 31),
	(38, 16, 'LIKE', 6),
	(39, 16, 'LIKE', 6),
	(40, 16, 'DISLIKE', 1),
	(41, 16, 'LIKE', 32),
	(42, 16, 'DISLIKE', 8),
	(43, 16, 'LIKE', 7),
	(44, 16, 'LIKE', 33),
	(45, 16, 'LIKE', 35),
	(46, 16, 'LIKE', 3),
	(47, 16, 'LIKE', 3),
	(48, 16, 'LIKE', 37),
	(49, 16, 'DISLIKE', 3),
	(50, 16, 'LIKE', 41),
	(51, 16, 'DISLIKE', 3),
	(52, 16, 'LIKE', 42),
	(53, 16, 'DISLIKE', 3),
	(54, 16, 'LIKE', 1),
	(55, 16, 'LIKE', 44),
	(56, 16, 'LIKE', 1);
/*!40000 ALTER TABLE `assessments` ENABLE KEYS */;

-- Dumping structure for πίνακας traffic.events
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `issue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `issue_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('CRITICAL','MAJOR','MINOR') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `events_ibfk_1` (`user_id`) USING BTREE,
  CONSTRAINT `events_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table traffic.events: ~2 rows (approximately)
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` (`id`, `user_id`, `issue`, `issue_date`, `type`) VALUES
	(5, 16, 'Test a', '2018-05-09 18:54:47', 'MAJOR'),
	(6, 16, 'Test B', '2018-05-09 18:55:06', 'CRITICAL'),
	(7, 16, 'Car accident', '2018-05-09 18:55:19', 'CRITICAL');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

-- Dumping structure for πίνακας traffic.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `issue` varchar(255) DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table traffic.reports: ~14 rows (approximately)
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` (`id`, `issue`, `issue_date`, `lat`, `lng`, `user_id`) VALUES
	(1, 'Car accident', '2018-05-09 19:13:07', 39.6616, 20.8511, 16),
	(2, 'Pedestrian injury', '2018-05-09 19:13:33', 39.6429, 20.8538, 16),
	(3, 'Βroken tree', '2018-05-09 19:16:05', 39.6429, 20.8538, 16),
	(42, 'Video test event', '2018-05-23 18:56:17', 39.6543, 20.8588, 16),
	(43, 'asdasdf', '2018-05-23 20:00:12', 40.6401, 22.9444, 16),
	(44, 'afsdasfdb', '2018-05-23 20:26:52', 39.6544, 20.8588, 16),
	(45, 'xvsdfhdfgh', '2018-05-23 20:28:07', 39.6543, 20.8588, 16);
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Dumping structure for πίνακας traffic.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table traffic.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`) VALUES
	(1, 'ADMIN'),
	(2, 'CITIZEN'),
	(3, 'SUPER USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for πίνακας traffic.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `surname` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `date_of_birthday` date DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table traffic.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `name`, `surname`, `date_of_birthday`) VALUES
	(1, 'netheodw', 'abcd1234', 'Test', 'Test2', '2011-02-11'),
	(16, 'Jim', '1234', 'User', '1', '2010-03-01');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for πίνακας traffic.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UKa9dydk3dj4qb8cvmjijqnrg5t` (`user_id`,`role_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table traffic.user_roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`id`, `user_id`, `role_id`) VALUES
	(2, 1, 1),
	(7, 16, 2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
